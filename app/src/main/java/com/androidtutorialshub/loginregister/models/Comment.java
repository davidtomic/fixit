package com.androidtutorialshub.loginregister.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomicdavid on 10/19/17.
 */

public class Comment {

    @SerializedName("id")
    private int comment_id;
    @SerializedName("comment_user_email")
    private String comment_user_email;
    @SerializedName("commentOwner")
    private String commentOwner;
    @SerializedName("body")
    private String body;

    public int getComment_id() {
        return comment_id;
    }

    public void setComment_id(int comment_id) {
        this.comment_id = comment_id;
    }

    public String getComment_user_email() {
        return comment_user_email;
    }

    public void setComment_user_email(String comment_user_email) {
        this.comment_user_email = comment_user_email;
    }

    public String getCommentOwner() {
        return commentOwner;
    }

    public void setCommentOwner(String commentOwner) {
        this.commentOwner = commentOwner;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}