package com.androidtutorialshub.loginregister.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomicdavid on 10/20/17.
 */

public class Message {

    @SerializedName("message_id")
    private int messageId;
    @SerializedName("message_sender")
    private String messageSender;
    @SerializedName("message_reciever")
    private String messageReciever;
    @SerializedName("message_body")
    private String messageBody;
    @SerializedName("message_time")
    private String messageTime;

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessageSender() {
        return messageSender;
    }

    public void setMessageSender(String messageSender) {
        this.messageSender = messageSender;
    }

    public String getMessageReciever() {
        return messageReciever;
    }

    public void setMessageReciever(String messageReciever) {
        this.messageReciever = messageReciever;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }
}
