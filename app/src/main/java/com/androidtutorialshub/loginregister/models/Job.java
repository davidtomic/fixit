package com.androidtutorialshub.loginregister.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomicdavid on 10/23/17.
 */

public class Job {

    @SerializedName("job_id")
    private int jobId;
    @SerializedName("job_owner")
    private String jobOwner;
    @SerializedName("job_location")
    private String jobLocation;
    @SerializedName("job_phone_number")
    private String jobPhoneNumber;
    @SerializedName("job_title")
    private String jobTitle;
    @SerializedName("job_description")
    private String jobDescription;

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public String getJobOwner() {
        return jobOwner;
    }

    public void setJobOwner(String jobOwner) {
        this.jobOwner = jobOwner;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getJobPhoneNumber() {
        return jobPhoneNumber;
    }

    public void setJobPhoneNumber(String jobPhoneNumber) {
        this.jobPhoneNumber = jobPhoneNumber;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }
}
