package com.androidtutorialshub.loginregister.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomicdavid on 10/17/17.
 */

public class Master {

    @SerializedName("master_id")
    private int id;
    @SerializedName("master_name")
    private String name;
    @SerializedName("master_email")
    private String email;
    @SerializedName("master_password")
    private String password;
    @SerializedName("master_businesses")
    private String businesses;
    @SerializedName("master_have_licence")
    private String haveLicence;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBussiness() {
        return businesses;
    }

    public void setBussiness(String businesses) {
        this.businesses = businesses;
    }

    public String haveLicence() {
        return haveLicence;
    }

    public void setHaveLicence(String haveLicence) {
        this.haveLicence = haveLicence;
    }
}
