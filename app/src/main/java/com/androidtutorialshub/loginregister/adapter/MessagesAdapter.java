package com.androidtutorialshub.loginregister.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.activities.MasterActivity;
import com.androidtutorialshub.loginregister.activities.MessagesActivity;
import com.androidtutorialshub.loginregister.models.Message;

import java.util.List;

/**
 * Created by tomicdavid on 10/23/17.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder>{

    private List<Message> list;
    private Context context;

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView _client;
        TextView _body;

        public ViewHolder(View itemView) {
            super(itemView);

            _client = (TextView)itemView.findViewById(R.id.message_client);
            _body = (TextView)itemView.findViewById(R.id.message_preview);

            _client.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            _body.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MessagesActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("email", _client.getText().toString());
                    context.startActivity(intent);
                }
            });
        }
    }

    public MessagesAdapter(List<Message> list, Context context){
        this.list = list;
        this.context = context;
    }

    @Override
    public MessagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_messages, parent, false);
        return new MessagesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Message message = list.get(position);

        String client = message.getMessageSender();
        String body = message.getMessageBody();

        holder._client.setText(client);
        holder._body.setText(body);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
