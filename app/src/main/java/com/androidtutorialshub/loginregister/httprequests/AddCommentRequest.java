package com.androidtutorialshub.loginregister.httprequests;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by tomicdavid on 10/23/17.
 */

public class AddCommentRequest extends AsyncTask<String, Void, Void> {

    @Override
    protected Void doInBackground(String... params) {
        OutputStream os = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL("http://192.168.30.245/fixit/comments/add_comments.php");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("comment_owner", params[0]);
            jsonObject.put("comment_user_email", params[1]);
            jsonObject.put("body", params[2]);
            String message = jsonObject.toString();

            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            //make some HTTP header nicety
            conn.setRequestProperty("Content-Type", "application/json");

            //open
            conn.connect();

            //send data
            os = new BufferedOutputStream(conn.getOutputStream());
            os.write(message.getBytes());

            //clean up
            os.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

}
