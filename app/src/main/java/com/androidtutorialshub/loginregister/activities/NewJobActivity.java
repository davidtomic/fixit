package com.androidtutorialshub.loginregister.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.httprequests.NewJobRequest;

/**
 * Created by tomicdavid on 10/23/17.
 */

public class NewJobActivity extends Activity{

    private String credentialEmail;

    private TextView job_user;
    private TextView job_location;
    private TextView job_number;

    private EditText job_title;
    private EditText job_description;

    private Button applyJob;

    private Context context = null;

    NewJobRequest newJobRequest = new NewJobRequest();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_job);

        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        credentialEmail = prefs.getString("credentials", "default_value_here_if_string_is_missing");

        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        context = getApplicationContext();

        job_user = (TextView)findViewById(R.id.job_username);
        job_number = (TextView)findViewById(R.id.job_number);
        job_location = (TextView)findViewById(R.id.job_location);

        job_title = (EditText)findViewById(R.id.job_name);
        job_description = (EditText)findViewById(R.id.job_description);

        applyJob = (Button)findViewById(R.id.apply_job);

        job_user.setText(credentialEmail);
        job_number.setText("+385 99 2966 677");
        job_location.setText("Trešnjevka, 10000 Zagreb, Hrvatska");

        applyJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newJobRequest.execute(job_user.getText().toString(), job_location.getText().toString(), job_number.getText().toString(), job_title.getText().toString(), job_description.getText().toString());
                Intent intent = new Intent(context, AllJobsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.all_jobs:
                startActivity(new Intent(this, AllJobsActivity.class));
                return true;
            case R.id.logout:
                SharedPreferences sharedPref = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("credentials", "");
                editor.apply();
                editor.commit();
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
