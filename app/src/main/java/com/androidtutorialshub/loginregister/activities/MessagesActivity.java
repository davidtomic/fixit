package com.androidtutorialshub.loginregister.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.adapter.MessagesAdapter;
import com.androidtutorialshub.loginregister.httprequests.GetMessagesRequest;
import com.androidtutorialshub.loginregister.models.Message;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by tomicdavid on 10/22/17.
 */

public class MessagesActivity extends Activity {

    private RecyclerView mRecyclerView;

    private List<Message> messages;

    private String credentialEmail;

    GetMessagesRequest messageRequests = new GetMessagesRequest();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        mRecyclerView = (RecyclerView) findViewById(R.id.messages_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        credentialEmail = prefs.getString("credentials", "default_value_here_if_string_is_missing");

        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        try {
            loadMessages();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mRecyclerView.setAdapter(new MessagesAdapter(messages, getApplicationContext()));

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    public void loadMessages() throws Exception {

        String messageResponse = "";

        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        String string = prefs.getString("credentials", "default_value_here_if_string_is_missing");

        messageResponse = messageRequests.execute(string).get();

        Type listType = new TypeToken<List<Message>>(){
        }.getType();

        messages = new Gson().fromJson(messageResponse, listType);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.all_jobs:
                startActivity(new Intent(this, AllJobsActivity.class));
                return true;
            case R.id.logout:
                SharedPreferences sharedPref = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("credentials", "");
                editor.apply();
                editor.commit();
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
