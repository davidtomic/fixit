package com.androidtutorialshub.loginregister.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.androidtutorialshub.loginregister.adapter.MasterListAdapter;
import com.androidtutorialshub.loginregister.httprequests.GetAllMasters;
import com.androidtutorialshub.loginregister.models.Master;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.androidtutorialshub.loginregister.R;

/**
 * Created by tomicdavid on 10/17/17.
 */

public class MasterListActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;

    Context context = this;

    private ImageButton homeButton;
    private ImageButton messageButton;
    private ImageButton cameraButton;

    private String credentialEmail;

    private View bottomToolbar;

    List<Master> mastersList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_masters);

        mRecyclerView = findViewById(R.id.masters_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        credentialEmail = prefs.getString("credentials", "default_value_here_if_string_is_missing");

        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        bottomToolbar = findViewById(R.id.bottom_toolbar_master);

        homeButton = bottomToolbar.findViewById(R.id.home_button);
        messageButton = bottomToolbar.findViewById(R.id.message_button);
        cameraButton = bottomToolbar.findViewById(R.id.comment_button);

        cameraButton.setImageResource(R.drawable.camerabutton);

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MasterListActivity.class);
                startActivity(intent);
            }
        });

        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MessagesActivity.class);
                startActivity(intent);
            }
        });

        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NewJobActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        GetAllMasters httpRequests = new GetAllMasters();

        String jsonString = null;

        try {
            jsonString = httpRequests.execute().get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Type listType = new TypeToken<List<Master>>(){
        }.getType();

        mastersList = getPostListFromJson(jsonString, listType);

        mRecyclerView.setAdapter(new MasterListAdapter(mastersList, getApplicationContext()));

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    public static <T> List<T> getPostListFromJson(String jsonString, Type type){
        if(!isValid(jsonString)){
            return null;
        }
        return new Gson().fromJson(jsonString, type);
    }

    public static boolean isValid(String json){
        try{
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.all_jobs:
                startActivity(new Intent(this, AllJobsActivity.class));
                return true;
            case R.id.logout:
                SharedPreferences sharedPref = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("credentials", "");
                editor.apply();
                editor.commit();
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}