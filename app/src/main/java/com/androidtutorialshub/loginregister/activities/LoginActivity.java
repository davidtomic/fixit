package com.androidtutorialshub.loginregister.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.httprequests.LoginRequests;

import java.util.concurrent.ExecutionException;

/**
 * Created by tomicdavid on 10/17/17.
 */

public class LoginActivity extends AppCompatActivity {

    private EditText email;
    private EditText password;

    private String loginEmail;
    private String loginPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_master);
        final Context context = getApplicationContext();

        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        String string = prefs.getString("credentials", "default_value_here_if_string_is_missing");

        if(string.length()>0){
            Intent intent = new Intent(context, MasterListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        email = (EditText)findViewById(R.id.login_email);
        password = (EditText)findViewById(R.id.login_password);
        final Button loginButton = (Button)findViewById(R.id.login_button);
        final TextView loginToRegisterButton = (TextView)findViewById(R.id.login_to_register_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                loginEmail = email.getText().toString();
                loginPassword = password.getText().toString();
                if(!loginEmail.isEmpty() && !loginPassword.isEmpty()){
                    try {
                        LoginRequests loginRequests = new LoginRequests();
                        if(loginRequests.execute(loginEmail, loginPassword).get()){
                            SharedPreferences sharedPref = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString("credentials", loginEmail);
                            editor.apply();
                            editor.commit();
                            Intent intent = new Intent(context, MasterListActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        loginToRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RegisterActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        String string = prefs.getString("credentials", "default_value_here_if_string_is_missing");

        if(string.length()>0){
            Intent intent = new Intent(getApplicationContext(), MasterListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        String string = prefs.getString("credentials", "default_value_here_if_string_is_missing");

        if(string.length()>0){
            Intent intent = new Intent(getApplicationContext(), MasterListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        String string = prefs.getString("credentials", "default_value_here_if_string_is_missing");

        if(string.length()>0){
            Intent intent = new Intent(getApplicationContext(), MasterListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

}
