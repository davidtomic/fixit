package com.androidtutorialshub.loginregister.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.adapter.JobListAdapter;
import com.androidtutorialshub.loginregister.httprequests.GetAllJobsRequest;
import com.androidtutorialshub.loginregister.models.Job;
import com.androidtutorialshub.loginregister.models.Master;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by tomicdavid on 10/24/17.
 */

public class AllJobsActivity extends Activity {

    private String credentialEmail;
    private RecyclerView jobListView;

    private List<Job> jobs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_jobs);

        Context context = getApplicationContext();

        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        credentialEmail = prefs.getString("credentials", "default_value_here_if_string_is_missing");

        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        jobListView = (RecyclerView)findViewById(R.id.item_job_list);
        jobListView.setLayoutManager(new LinearLayoutManager(this));

        GetAllJobsRequest getAllJobsRequest = new GetAllJobsRequest();

        String response = "";

        try {
            response = getAllJobsRequest.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Type listType = new TypeToken<List<Job>>(){
        }.getType();

        jobs = getPostListFromJson(response, listType);

        jobListView.setAdapter(new JobListAdapter(jobs, context));

    }

    public static <T> List<T> getPostListFromJson(String jsonString, Type type){
        if(!isValid(jsonString)){
            return null;
        }
        return new Gson().fromJson(jsonString, type);
    }

    public static boolean isValid(String json){
        try{
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.all_jobs:
                startActivity(new Intent(this, MasterListActivity.class));
                return true;
            case R.id.logout:
                SharedPreferences sharedPref = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("credentials", "");
                editor.apply();
                editor.commit();
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
